import sun.security.util.BitArray;

import java.io.FileWriter;
import java.io.IOException;
import java.util.BitSet;
import java.util.HashSet;

/**
 * Created by chayma on 3/20/17.
 */
public class Main {

    public static void main(String[] args) {
        int flip=70, nbRechercheLocales=30, distance=1, maxChances=5;
        long maxIteration=500;//fdebut kont dayra long.maxvalue
        System.out.println("flip="+flip+"  nbRechercheLocales="+nbRechercheLocales+"  distance="+distance+
                            "  maxIterations="+maxIteration+"  maxChances="+maxChances);
        for (int i=1; i<11; i++) {
            try {
                CNF cnf = new CNF("files/uf75-0" + i + ".cnf");
//                CNF cnf = new CNF("me.cnf");
                BSO bso = new BSO(cnf, flip, nbRechercheLocales, distance, maxChances, maxIteration);
                double debut= System.currentTimeMillis();
                Bee bee=bso.rechercher();
                double temps= (System.currentTimeMillis()-debut)* 0.001;
                FileWriter fileWriter= new FileWriter(flip+"_"+nbRechercheLocales+"_"+distance+"_"+maxIteration+"_"+maxChances, true);
                fileWriter.write(i+"\t"+ bee.getFitness()+"\t"+temps+"\n");
                fileWriter.close();
                System.out.println(i+"\t"+ bee.getFitness()+"\t"+temps);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
