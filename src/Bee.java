import java.util.*;

/**
 * Created by chayma on 3/22/17.
 */
public class Bee implements Comparable<Bee>{

    private BitSet interpretation;//on peut la renommer
    private int fitness; // pour stocker la dance de l'abeille
    private static CNF champ;

    public Bee(CNF champ) {//creer une abeille eclaireuse
        //interpretation = new BitSet(champ.getNombreVariable());
        Bee.champ = champ;
        interpretation = initializeBee(); //initialier aleatoirement (elle est en commentaire just pour le test
        dancer();
    }

    public Bee(Bee bee, int nbflip){
        interpretation= (BitSet)bee.interpretation.clone();
        for (int i=0; i< nbflip; i++){
            interpretation.flip((int) (Math.random()*100 %champ.getNombreVariable()));
        }
        dancer();
    }

    public Bee(Bee bee){
        interpretation= (BitSet)bee.interpretation.clone();
        dancer();
    }
    private Bee(Bee b,int debutFlip,int flip) {//creation d'une abeille qui va flipper
        interpretation= (BitSet)b.interpretation.clone();
        for (int i=debutFlip; i< champ.getNombreVariable(); i+=flip){
            interpretation.flip(i);
        }
        dancer();
    }

    private BitSet initializeBee(){
        BitSet rnd = new BitSet();
        for (int i=0; i< champ.getNombreVariable(); i++){
            if(Math.random()> 0.5){
                rnd.flip(i);
            }
        }
        return rnd;
    }

    public HashSet<Bee> callBees(int flip){//crée K solutions avec l'operateur flip
        HashSet<Bee> bees= new HashSet<>();
        for (int i=0; i< flip; i++){
            bees.add(new Bee(this,i, flip));
        }
        return bees;
    }

    public void dancer(){//l'abeille cherche de la nourriture dans un champ :v
        //c'est la fonction fitness
        HashSet<Clause> clauses= champ.getClauses();
        int fitness=0;
        for (Clause clause: clauses){
            for (int variable: clause.getLitteraux()){
                if(interpretation.get(Math.abs(variable)-1)){
                    if (variable>0){
                        fitness++;
                        break;
                    }
                }
                else{
                    if (variable<0){
                        fitness++;
                        break;
                    }
                }
            }
        }
         this.fitness = fitness; // sauvgarder la dance
    }

    @Override
    public String toString() {
        return "Bee{" +
                "interpretation=" + interpretation +
                ", fitness=" + fitness +
                '}';
    }
    public BitSet getInterpretation() {
        return interpretation;
    }

    public void setInterpretation(BitSet interpretation) {
        this.interpretation = interpretation;
    }

    public static int getTaille() {
        return champ.getNombreVariable();
    }

    public int getFitness() {
        return fitness;
    }

    public void setFitness(int fitness) {
        this.fitness = fitness;
    }

    @Override
    public int compareTo(Bee bee) {
        return fitness - bee.fitness; // compareTo doit retourner  1 / 0 / -1
    }

    @Override
    public int hashCode() {
        return interpretation.hashCode();
    }
    @Override
    public boolean equals(Object o) {
        return interpretation.equals(((Bee)o).interpretation);
    }

    public Bee maxRechercheLocale(int nbRecherchesLocales, int distance){
        Bee beeMax=this;
        for (long i=0; i<nbRecherchesLocales; i++){
            Bee bee = new Bee(this, distance);
            if(beeMax.compareTo(bee)<0){
                beeMax= bee;
            }
        }
        return beeMax;
    }

    public int diversity(HashSet<Bee> taboo){
        int min=champ.getNombreVariable();
        for (Bee b: taboo){
            BitSet bee = (BitSet) interpretation.clone();
            bee.xor(b.getInterpretation());
            int temp = bee.cardinality();
            if(temp<min) min=temp;
        }
        return min;
    }

}
