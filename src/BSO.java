import java.util.BitSet;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;

/**
 * Created by chayma on 3/22/17.
 */
public class BSO {
    private HashSet<Bee> tableDances;
    private CNF cnf;
    private HashSet<Bee> taboo;//tableau d'interpretation visitée
    private int flip, nbRecherchesLocales, distance, maxChances;
    private long maxIterations;

    public BSO(CNF cnf, int flip, int nbRecherchesLocales, int distance, int maxChances, long maxIterations) {
        this.cnf = cnf;
        tableDances = new HashSet<>();
        taboo = new HashSet<>();
        this.flip = flip;
        this.nbRecherchesLocales = nbRecherchesLocales;
        this.distance = distance;
        this.maxIterations = maxIterations;
        this.maxChances = maxChances;
    }

    public Bee rechercher(){
        int numberChances = maxChances;
        Bee eclaireuse= new Bee(cnf);
        int iterations=0;
        boolean notFound;
        if(eclaireuse.getFitness()== cnf.getNombreClauses()){
            notFound=false;
        }
        else{
            notFound= true;
        }
        while (iterations< maxIterations){
            tableDances = new HashSet<>();
            iterations++;
            taboo.add(eclaireuse);
            HashSet<Bee> kbees= eclaireuse.callBees(flip);
            for (Bee k: kbees){
                //recherche local:
                tableDances.add(k.maxRechercheLocale(nbRecherchesLocales, distance));
            }
            Bee maxDance= Collections.max(tableDances);

            if(maxDance.getFitness() == cnf.getNombreClauses()){
                return maxDance;
            }
            else{
                while (taboo.contains(maxDance) && !tableDances.isEmpty()){
                    tableDances.remove(maxDance);
                    maxDance= Collections.max(tableDances);
                }

                if(maxDance.compareTo(eclaireuse)>0){
                    eclaireuse=maxDance;
                    if(numberChances != maxChances) numberChances =maxChances;

                }else{
                    numberChances--;
                    if(numberChances > 0){
                        eclaireuse = maxDance;
                    }else{
                        eclaireuse = bestInDiversity();
                        numberChances = maxChances;
                    }
                }
                tableDances.remove(maxDance);
            }
        }

        return Collections.max(taboo);
    }

    public Bee bestInDiversity(){

        Bee bestBee = Collections.max(tableDances, new Comparator<Bee>() {
            @Override
            public int compare(Bee bee, Bee t1) {
                return (bee.diversity(taboo)-t1.diversity(taboo));
            }
        });
        return bestBee;
    }

    public void setTableDances(HashSet<Bee> tableDances) {
        this.tableDances = tableDances;
    }
}
