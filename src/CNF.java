import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by dzcod3r on 3/4/17.
 */
public class CNF {
    private HashSet<Clause> clauses;
    private String cheminDuFichier;
    private int nombreVariables, nombreClauses;

    private void lireLesClauses() throws IOException{

        String headerReg= "p[ \\t\\n\\x0B\\f\\r]+cnf[ \\t\\n\\x0B\\f\\r]+([0-9]+)[ \\t\\n\\x0B\\f\\r]+([0-9]+)";
        String clauseReg= "(.*)[ \\t\\n\\x0B\\f\\r]+0";//"[ \\t\\n\\x0B\\f\\r]*(.+)[ \\t\\n\\x0B\\f\\r]+0[ \\t\\n\\x0B\\f\\r]+";
        Boolean headerFound= false;

        BufferedReader bufferedReader;

            bufferedReader= new BufferedReader(new FileReader(cheminDuFichier));
            String buffer;

            while (!headerFound && (buffer = bufferedReader.readLine()) != null) {
                //get header:
                Pattern p = Pattern.compile(headerReg);
                Matcher m = p.matcher(buffer);
                if(m.find() && m.groupCount()>0){
                    headerFound=true;
                    nombreVariables=Integer.parseInt(m.group(1));
                    nombreClauses= Integer.parseInt(m.group(2));

                }

            }

            if(headerFound){
                clauses = new HashSet<>();
                int cptClauses=0;
                while (cptClauses != nombreClauses && (buffer = bufferedReader.readLine()) != null){
                    //get clauses:
                    Pattern p = Pattern.compile(clauseReg);
                    Matcher m = p.matcher(buffer);
                    if(m.find() && m.groupCount()>0){
                        cptClauses++;
                        String clause= m.group(1);

                        //get literals:
                        clause =clause.trim();
                        String [] literals= clause.split("[ \\t\\n\\x0B\\f\\r]+");
                        HashSet<Integer> uneClause=new HashSet<>();
                        for (int i=0; i< literals.length; i++){
                            uneClause.add(Integer.valueOf(literals[i]));
                        }
                        clauses.add(new Clause(uneClause));
                    }
                }
            }
            bufferedReader.close();
    }


    //setters + getters
    public CNF(String cheminDuFichier) throws IOException {
        this.cheminDuFichier = cheminDuFichier;
        clauses = new HashSet<>();
        lireLesClauses();

    }

    public HashSet<Clause> getClauses() {
        return clauses;
    }

    public void setClauses(HashSet<Clause> clauses) {
        this.clauses = clauses;
    }

    public String getCheminDuFichier() {
        return cheminDuFichier;
    }

    public void setCheminDuFichier(String cheminDuFichier) {
        this.cheminDuFichier = cheminDuFichier;
    }

    public int getNombreVariable() {
        return nombreVariables;
    }

    public void setNombreVariable(int nombreVariables) {
        this.nombreVariables = nombreVariables;
    }

    public int getNombreClauses() {
        return nombreClauses;
    }

    public void setNombreClauses(int nombreClauses) {
        this.nombreClauses = nombreClauses;
    }

    @Override
    public String toString() {
        return "CNF{" +
                "clauses=" + clauses +
                ", cheminDuFichier='" + cheminDuFichier + '\'' +
                ", nombreVariable=" + nombreVariables +
                ", nombreClauses=" + nombreClauses +
                '}';
    }
}
